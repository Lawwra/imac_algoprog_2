#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    // your code
};

struct DynaTableau{
    int* donnees;
    int taille;
    int capacite;
    // your code
};


void initialise(Liste* liste)
{
    liste -> premier = NULL;
}

bool est_vide(const Liste* liste)
{
    if(liste -> premier == NULL)
        return true;
    else
        return false;
}

void ajoute(Liste* liste, int valeur)
{
    Noeud* noeud_new = (Noeud*) malloc(sizeof(Noeud));
    noeud_new -> donnee = valeur;
    noeud_new -> suivant = NULL;

    if(liste -> premier == NULL)
    {
        liste -> premier = noeud_new;
    }
    else
    {
        Noeud* noeud_act = liste -> premier;
        while(noeud_act -> suivant != NULL)
        {
            noeud_act = noeud_act -> suivant;
        }

        noeud_act -> suivant = noeud_new;
    }
}

void affiche(const Liste* liste)
{
    Noeud* noeud_act = liste -> premier;
    while(noeud_act != NULL)
    {
        cout << " " << noeud_act -> donnee;
        noeud_act = noeud_act -> suivant;
    }
    cout << endl;
}

int recupere(const Liste* liste, int n)
{
    Noeud* noeud_act = liste -> premier;
    for(int i=0; i<n; i++)
    {
        noeud_act = noeud_act -> suivant;
    }
    return noeud_act -> donnee;
}

int cherche(const Liste* liste, int valeur)
{
    Noeud* noeud_act = liste -> premier;
    int index = 0;
    while(noeud_act != NULL)
    {
        if(noeud_act -> donnee == valeur)
        {
            return index;
        }
        noeud_act = noeud_act -> suivant;
        index++;
    }
    return -1;
}

void stocke(Liste* liste, int n, int valeur)
{
    Noeud* noeud_act = liste -> premier;
    for(int i=0; i<n; i++)
    {
        noeud_act = noeud_act -> suivant;
    }
    noeud_act -> donnee = valeur;
}

void ajoute(DynaTableau* tableau, int valeur)
{
    // cas où on dépasse le tableau
    if(tableau -> taille == tableau -> capacite)
    {
        tableau -> capacite ++;
        tableau -> donnees = (int*) realloc(tableau -> donnees, sizeof(int)*tableau -> capacite);
    }
    tableau -> donnees[tableau -> taille] = valeur;
    tableau -> taille++;
}


void initialise(DynaTableau* tableau, int capacite)
{
    tableau -> capacite = capacite;
    tableau -> taille = 0;
    tableau -> donnees = (int*) malloc(sizeof(int) * tableau -> capacite);
}

bool est_vide(const DynaTableau* tableau)
{
    for(int i=0; i < tableau -> taille; i++)
    {
        if(tableau -> donnees[i] != NULL)
            return false;
    }
    return true;
}

void affiche(const DynaTableau* tableau)
{
    for(int i=0; i < tableau -> taille; i++)
    {
        cout << " " << tableau -> donnees[i];
    }
}

int recupere(const DynaTableau* tableau, int n)
{
    return tableau -> donnees[n];
}

int cherche(const DynaTableau* tableau, int valeur)
{
    for(int i=0; i < tableau -> taille; i++)
    {
       if(tableau -> donnees[i] == valeur)
           return i;
    }
    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    tableau -> donnees[n] = valeur;
}

void pousse_file(DynaTableau* tableau, int valeur)
//void pousse_file(Liste* liste, int valeur)
{
    ajoute(tableau, valeur);
}

int retire_file(DynaTableau* tableau)
//int retire_file(Liste* liste)
{
    int donnee_sup = tableau -> donnees[0];
    for(int i=0; i < tableau -> taille; i++)
    {
        tableau -> donnees[i] =  tableau -> donnees[i+1];
    }
    tableau -> taille--;
    return donnee_sup;
}

void pousse_pile(DynaTableau* tableau, int valeur)
//void pousse_pile(Liste* liste, int valeur)
{
    if(tableau -> taille == tableau -> capacite)
    {
        tableau -> capacite ++;
        tableau -> donnees = (int*) realloc(tableau -> donnees, sizeof(int)*tableau -> capacite);
    }

    for(int i=tableau -> taille; i >= 0; i--)
    {
        tableau -> donnees[i+1] =  tableau -> donnees[i];
    }
    tableau -> donnees[0] = valeur;
    tableau -> taille++;
}

int retire_pile(DynaTableau* tableau)
//int retire_pile(Liste* liste)
{
    int donnee_sup = tableau -> donnees[0];
    for(int i=0; i < tableau -> taille; i++)
    {
        tableau -> donnees[i] =  tableau -> donnees[i+1];
    }
    tableau -> taille--;
    return donnee_sup;
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    DynaTableau pile; //Liste pile;
    DynaTableau file; //Liste file;

    initialise(&pile, 7);
    initialise(&file, 7);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
