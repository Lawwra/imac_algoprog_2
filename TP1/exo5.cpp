#include "tp1.h"
#include <QApplication>
#include <time.h>

int isMandelbrot(Point z, int n, Point point){
    // recursiv Mandelbrot
    if (n>0) {
        Point New_z;
        New_z.x = z.x*z.x - z.y*z.y + point.x;
        New_z.y = 2*(z.x)*(z.y) + point.y;
        if (sqrt(New_z.x*New_z.x + New_z.y*New_z.y) >2) {
            return 0;
        }
        else {
            return (isMandelbrot(New_z, n-1, point));
        }
    }

    return 1;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



